#ifndef MARENA_H
#define MARENA_H


#include <map>
#include <unordered_map>
#include <vector>
#include <queue>
#include <fstream>
#include <jemalloc/jemalloc.h>

#include "utils/timer.hpp"
#include "utils/mutex.hpp"


class ProfileThread;
class AGraph;

typedef int arena_id_t;
typedef unsigned ap_id_t;
typedef std::string string;

struct APInfo {
  int id;
  int arena_id;
  size_t size;
  APInfo(int i): id(i), arena_id(-1), size(0) {}
};

struct PtrRecord {
  void* key;
  size_t size;
  //AllocPoint* ap_info;
  ap_id_t ap_id;
};

struct ArenaInfo {
  arena_id_t arena_id;
  int ap_id;
  unsigned long long accesses;
  size_t size;
  size_t nchunk;
  size_t nalloc;
  size_t nfree;

  ArenaInfo(): arena_id(-1), ap_id(-1), accesses(0), size(0), nchunk(0), nalloc(0), nfree(0) {}
  ArenaInfo(arena_id_t id): arena_id(id), ap_id(-1), accesses(0), size(0), nchunk(0), nalloc(0), nfree(0) {}
};

struct ChunkInfo {
  ArenaInfo* arena;
  void* start;
  size_t size;

  ChunkInfo(): arena(NULL), start(NULL), size(0) {}
};

class Marena {
  bool _is_active;
  arena_id_t _cold_id;
  arena_id_t _hot_id;
  //int _arena_cnt;
  Timer _timer;

  /* flags */
  bool _record_ptrs;
  bool _record_arenas;
  bool _print_arena_id;
  bool _trace_alloc;
  bool _trace_mem;
  bool _debug_mem_trace;
  bool _guided;
  int _arena_mode;
  char _allocator;

  /* profiling */
  bool _shall_sample;
  size_t _mem_sample_rate;
  size_t _mem_access_count;
  AGraph* _graph;
  int _prev_ap;

  unsigned long long _alloc_bytes;

  //std::vector<AllocPoint*> _alloc_points;
  //std::unordered_map<ap_id_t, arena_id_t> _ap_arena_map;
  std::unordered_map<ap_id_t, APInfo*> _ap_map;
  //std::vector<arena_id_t> _arenas;
  std::unordered_map<void*, PtrRecord*> _ptr_records;
  std::unordered_map<arena_id_t, ArenaInfo*> _arena_info_map;
  std::vector<void*> _chunk_addrs;  // sorted, used for lowerbound lookup
  std::unordered_map<void*, ChunkInfo*> _chunk_info_map;
  void* _chunk_upper_bound;
  std::ofstream _alloc_log;
  std::ofstream _mem_log;
  extent_hooks_t *_mhook;

  Monitor* _profile_lock;
  std::queue<void*> _addr_queue;
  friend class ProfileThread;
  ProfileThread* _profile_thread;
  bool _done;
public:
  Marena();
  ~Marena();

  bool is_active()                                            { return _is_active; }
  void set_is_active(bool v=true)                             { _is_active = v; }

  std::unordered_map<arena_id_t, ArenaInfo*>& arena_info_map()          { return _arena_info_map; }

  unsigned long long alloc_bytes()                            { return _alloc_bytes; }

  void start_timer()                                          { _timer.start(); }
  void stop_timer()                                           { _timer.stop(); }
  double get_app_time()                                       { return _timer.seconds(); }
  void print_stats();
  void print_malloc_stats(void *cbopaque, const char *s);
  void print_proc_stats();
  void print_arena_aps();
  void log_alloc(PtrRecord* pr);
  void log_free(PtrRecord* pr);
  void record_mem_access(void* p, const char* comment);
  void record_load(void* p);
  void record_store(void* p);


  void* xps_malloc(ap_id_t ap_id, size_t size);
  void* xps_calloc(ap_id_t ap_id, size_t nmemb, size_t size);
  void* xps_realloc(ap_id_t ap_id, void *ptr, size_t size);
  void xps_free(void* ptr);

  void* ctx_malloc(size_t size);
  void* ctx_calloc(size_t nmemb, size_t size);
  void* ctx_realloc(void *ptr, size_t size);
  void ctx_free(void* ptr);

  arena_id_t create_arena();
  void register_alloc_point();
  arena_id_t get_arena_id(ap_id_t ap_id);
  ArenaInfo* get_arena_info(arena_id_t id);

  void add_chunk_info(ChunkInfo* ci);

  //AllocPoint* get_alloc_point_info(ap_id_t ap_id);

  void add_ptr_record(void* ptr, size_t size, ap_id_t ap_id);
  void remove_ptr_record(void* ptr);
  bool in_ptr_record(void* p);
};

#endif
