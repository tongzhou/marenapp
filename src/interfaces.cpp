//
// Created by tzhou on 9/27/17.
//

#include <cstdio>
#include <jemalloc/jemalloc.h>
#include <cstring>
#include "marena.hpp"
#include "interfaces.hpp"
#include "cxx_allocate.hpp"

extern "C" int __attribute__ ((constructor)) marena_init(void);
extern "C" int __attribute__ ((destructor))  marena_exit(void);

FILE *malloc_stats_fp;
Marena* marena = NULL;

extern "C" {

#define DECLARE_TOOL(tool) void init_##tool(); void exit_##tool()
#define INIT_TOOL(tool) init_##tool()
#define EXIT_TOOL(tool) exit_##tool()

  // Tools declaration
  DECLARE_TOOL(ic_profile);
  DECLARE_TOOL(pcce);
  DECLARE_TOOL(hcce);
  DECLARE_TOOL(scce);
  
  void print_malloc_stats(void *cbopaque, const char *s) {
    fwrite(s, strlen(s), 1, malloc_stats_fp);
  }
  
  int marena_init() {
    marena = new Marena();
    marena->start_timer();

    // Initialize tools
    INIT_TOOL(ic_profile);
    INIT_TOOL(pcce);
    INIT_TOOL(scce);
  }

  int marena_exit() {
    // Exit tools
    EXIT_TOOL(ic_profile);
    EXIT_TOOL(pcce);
    EXIT_TOOL(scce);

    marena->stop_timer();
    const char* malloc_stats_file = "jemalloc_stats.out";
    if (malloc_stats_file != NULL) {
      malloc_stats_fp = fopen(malloc_stats_file, "w");

      if (malloc_stats_fp == NULL) {
        fprintf(stderr, "Error opening malloc_stats_file: %d\n");
      }

      fprintf(malloc_stats_fp,
              "AppRunTime: %.3f\n"
              "number of arenas: %zu\n"
              "allocated: %llu\n",
              marena->get_app_time(),
              marena->arena_info_map().size(),
              marena->alloc_bytes());
      je_malloc_stats_print(print_malloc_stats,NULL,NULL);
      fclose(malloc_stats_fp);
    }

    marena->print_stats();
    //marena->print_ptrs_by_arena();
    delete marena;
  }

  extern "C" void xps_malloc_log(int ap_id, size_t size, void* p) {

  }

  extern "C" void xps_record_load(void* p) {
    marena->record_load(p);
  }

  extern "C" void xps_record_store(void* p) {
    marena->record_store(p);
  }

  extern "C" void *ben_malloc(int ap_id, size_t size) {
    void* p;
    if (marena->is_active()) {
      p = marena->xps_malloc(ap_id, size);
    }
    else {
      p = malloc(size);
    }
    //xps_malloc_log(ap_id, size, p);
    return p;
  }

  extern "C" void *ben_calloc(int ap_id, size_t nmemb, size_t size) {
    if (marena->is_active()) {
      return marena->xps_calloc(ap_id, nmemb, size);
    }
    else {
      return calloc(nmemb, size);
    }
  }

  extern "C" void *ben_realloc(int ap_id, void *ptr, size_t size) {
    if (marena->is_active()) {
      return marena->xps_realloc(ap_id, ptr, size);
    }
    else {
      return realloc(ptr, size);
    }
  }

  extern "C" void ben_free(void* ptr) {
    if (marena->is_active()) {
      marena->xps_free(ptr);
    }
    else {
      free(ptr);
    }
  }


  /**@brief Indirect call handler
   *
   * The indi_xxx calls intercept the indirect calls
   * so that all alloc/free pairs in the source code are
   * handled by marena and
   * pointer recording is unnecessary
   * 0 is the reserved ap id for all indirect calls
   */
  void *indi_malloc(size_t size) {
    return ben_malloc(0, size);
  }

  void *indi_calloc(size_t nmemb, size_t size) {
    return ben_calloc(0, nmemb, size);
  }

  void *indi_realloc(void *ptr, size_t size) {
    return ben_realloc(0, ptr, size);
  }

  void indi_free(void* ptr) {
    ben_free(ptr);
  }


  extern "C" void acce_check_ctx(int level);
  extern "C" void hcce_check_ctx(int level);
  extern "C" void scce_check_ctx(int level);
  extern "C" void scce_detect_ctx(int level);
  extern "C" void pcce_check_ctx();
  extern "C" void pcce_detect_ctx(size_t id);
  extern "C" void ecce_detect_ctx(size_t id);


  void *ctx_malloc(size_t size) {
    return malloc(size);
  }

  void *ctx_calloc(size_t nmemb, size_t size) {
    return calloc(nmemb, size);
  }

  void *ctx_realloc(void *ptr, size_t size) {
    return realloc(ptr, size);
  }

  void ctx_free(void *ptr) {
    free(ptr);
  }

  void* ctx__Znam (size_t size) {
    return _Znam(size);
  }
  
  void* ctx__Znwm (size_t size) {
    return _Znwm(size);
  }
  
  void* ctx__ZdaPv (size_t size) {
    return _ZdaPv(size);
  }
  
  void* ctx__ZdlPv (size_t size) {
    return _ZdlPv(size);
  }

  void *pcce_malloc(size_t size) {
    pcce_detect_ctx(0);
    return malloc(size);
  }

  void *pcce_calloc(size_t nmemb, size_t size) {
    pcce_detect_ctx(1);
    return calloc(nmemb, size);
  }

  void *pcce_realloc(void *ptr, size_t size) {
    pcce_detect_ctx(2);
    return realloc(ptr, size);
  }

  void pcce_free(void *ptr) {
    free(ptr);
  }

  void* pcce__Znam (size_t size) {
    pcce_detect_ctx(3);
    return _Znam(size);
  }
  
  void* pcce__Znwm (size_t size) {
    pcce_detect_ctx(4);
    return _Znwm(size);
  }
  
  void* pcce__ZdaPv (size_t size) {
    return _ZdaPv(size);
  }
  
  void* pcce__ZdlPv (size_t size) {
    return _ZdlPv(size);
  }
  
  void *ecce_malloc(size_t size) {
    ecce_detect_ctx(0);
    return malloc(size);
  }

  void *ecce_calloc(size_t nmemb, size_t size) {
    ecce_detect_ctx(1);
    return calloc(nmemb, size);
  }

  void *ecce_realloc(void *ptr, size_t size) {
    ecce_detect_ctx(2);
    return realloc(ptr, size);
  }

  void ecce_free(void *ptr) {
    free(ptr);
  }

  void* ecce__Znam (size_t size) {
    ecce_detect_ctx(3);
    return _Znam(size);
  }
  
  void* ecce__Znwm (size_t size) {
    ecce_detect_ctx(4);
    return _Znwm(size);
  }
  
  void* ecce__ZdaPv (size_t size) {
    return _ZdaPv(size);
  }
  
  void* ecce__ZdlPv (size_t size) {
    return _ZdlPv(size);
  }

  void *scce_malloc(size_t size) {
    scce_detect_ctx(0);
    return malloc(size);
  }

  void *scce_calloc(size_t nmemb, size_t size) {
    scce_detect_ctx(1);
    return calloc(nmemb, size);
  }

  void *scce_realloc(void *ptr, size_t size) {
    scce_detect_ctx(2);
    return realloc(ptr, size);
  }

  void scce_free(void *ptr) {
    free(ptr);
  }

  void* scce__Znam (size_t size) {
    scce_detect_ctx(3);
    return _Znam(size);
  }
  
  void* scce__Znwm (size_t size) {
    scce_detect_ctx(4);
    return _Znwm(size);
  }
  
  void* scce__ZdaPv (size_t size) {
    return _ZdaPv(size);
  }
  
  void* scce__ZdlPv (size_t size) {
    return _ZdlPv(size);
  }

}
  
// void* _Znwm(size_t);
// void* _Znaj(size_t);
// void* _Znwj(size_t);
// void* _ZdaPv(size_t);
// void* _ZdlPv(size_t);

 
