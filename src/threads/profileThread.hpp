//
// Created by tzhou on 11/5/17.
//

#ifndef MSI_LISTENERTHREAD_HPP
#define MSI_LISTENERTHREAD_HPP

#include "thread.hpp"
#include "utils/macros.hpp"

class ProfileThread: public Thread {
public:
    ProfileThread();
    ~ProfileThread();

    void initialize();
    void finalize();

    void run() override;

    void process(void*p, const char* comment);
};

#endif //MSI_LISTENERTHREAD_HPP
