cmake_minimum_required(VERSION 3.5)
set(CMAKE_CXX_STANDARD 11)

set(SOURCES Runtime.cpp)
set(ThisTarget SCCE)
add_library(${ThisTarget} STATIC ${SOURCES})
target_compile_options(${ThisTarget} PRIVATE -fPIC -DZDEBUG -O3)
