//
// Created by tzhou on 3/8/18.
//

#include <fstream>
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <set>
#include <cstring>
#include <cassert>
#include <backtrace.h>
#include <backtrace-supported.h>
#include "marena.hpp"
#include "../CCECommons/Commons.hpp"
#include "../PCCE/Interface.hpp"

// ACCE
typedef char CtxSlotType;
typedef short EdgeType;
typedef size_t EdgeSlotType;

typedef tuple<int, size_t, size_t, vector<size_t>> HCCEKeyType;
//typedef tuple<int, size_t, vector<size_t>> HCCEKeyType;
// for fcce-naive.cfg
//typedef tuple<int, size_t, vector<EdgeType>> HCCEKeyType;
typedef tuple<int, size_t, size_t, size_t, vector<size_t>> SCCEKeyType;
const int ctx_size = 64;
extern "C" {
  size_t scce_ctx0;
  size_t scce_ctx1;
  size_t scce_ctx2;
  CtxSlotType scce_ctx[ctx_size];
  EdgeType* scce_start = 0;
  int scce_size = 0;
  int scce_capacity = 0;
  size_t scce_cycle_ctx = 0;
  size_t scce_cycle_ctx_size = 0;

  
  void scce_push_cycle_ctx(int bits, int value);
  void scce_pop_cycle_ctx(int id);
}

namespace {  
// Instru
unordered_map<size_t, int> _ctx_hashes;
vector<size_t > _cycle_edge_buffers;
vector<size_t > _scce_cycle_ctxs;
int _cycle_buffer_capacity;
// Unbuffered push
vector<EdgeType > _cycle_edges;

// Detection
map<HCCEKeyType, int> _fcce_ctx_pairs;
int _acyc_slots = 0;


//std::set<SCCEKeyType> _scce_ctx_set;
std::map<SCCEKeyType, int> _scce_ctx_pairs;
bool _justpushed = false;
size_t _ctx_count = 0;

// Debug
map<string, string> _ctxs;
map<string, string> _cvs;
map<int, size_t> _edge_freqs;
backtrace_state *_bt_state;
int _tz = 0;
int _naive_mode = 1;

// Stats
map<size_t, size_t> _stack_size_hist;
size_t _read_bytes[2] = {0};
size_t _pushed = 0;
size_t _popped = 0;
bool _use_sampling = false;
bool _dump_contexts = 0;
bool _load_contexts = false;
bool _dump_stack_hist = 0;
bool _dump_bit_stack_hist = false;

size_t _context_hit = 0;


string getSCCECtxAsStr(int level) {
  //printf("1\n");
  string s;
  for (int i = 0; i < level; ++i) {
    if (i) {
      s += "_";
    }
    char chars[10];
    sprintf(chars, "%04x", scce_ctx[i]);
    s += std::to_string(i) + "_" + chars;
  }

  s += "_cycle_";
  //printf("2\n");

  for (int i = 0; i < scce_size; ++i) {
    if (i) {
      s += "_";
    }
    char chars[10];
    sprintf(chars, "%04x", scce_start[i]);
    s += chars;
  }
  //printf("3\n");
  return s;
}

extern "C" size_t pcce_id;
  
void detectSCCECtx(size_t id) {
  //SCCEKeyType key(id, scce_ctx0);
  //SCCEKeyType key(id, scce_ctx0, scce_cycle_ctx, _scce_cycle_ctxs);


  //CAUTION
  // if (scce_cycle_ctx_size && scce_cycle_ctx_size == _cycle_buffer_capacity) {
    
  //   //assert(scce_cycle_ctx_size == 4);
  //   scce_push_cycle_ctx(0, 0);
  // }
  
  SCCEKeyType key(id, scce_ctx0, scce_ctx1, scce_cycle_ctx, _scce_cycle_ctxs);
  if (_scce_ctx_pairs.find(key) == _scce_ctx_pairs.end()) {
    _scce_ctx_pairs[key] = 0;
  }
}

void printCtxKey(HCCEKeyType key, FILE* f) {
  // fprintf(f, "%p %p|", get<0>(key), get<1>(key));
  // auto stack = get<3>(key);
  // if (!stack.empty()) {
  //   for (auto i: stack) {
  //       fprintf(f, "%p, ", i);
  //   }
  // }
  // fprintf(f, "\n");
}


size_t detectSCCEShiftCtx(int id) {
  if (_use_sampling) {
    if (_ctx_count != 0) {
      _ctx_count++;
      if (_ctx_count == 200000) {
        _ctx_count = 0;
      }
      return 0;
      }
    _ctx_count++;
  }
  
  
  size_t bytes = 0;

  //printf("capa: %d\n", _cycle_buffer_capacity);
  // if (scce_cycle_ctx_size && scce_cycle_ctx_size == _cycle_buffer_capacity) {
  //   scce_push_cycle_ctx(0, 0);
  // }
   

  HCCEKeyType key(id, pcce_id, scce_cycle_ctx, _scce_cycle_ctxs);
  // if (scce_cycle_ctx) {
  //   _scce_cycle_ctxs.push_back(scce_cycle_ctx);
  // }
  // HCCEKeyType key(id, pcce_id, _scce_cycle_ctxs);

  //HCCEKeyType key(id, pcce_id, _cycle_edges);


  if (_load_contexts) {
    if (_fcce_ctx_pairs.find(key) != _fcce_ctx_pairs.end()) {
      _context_hit++;
    }
  }
  else if (_dump_stack_hist) {
    int actual_words = _scce_cycle_ctxs.size();
    // if (actual_words == 0 && scce_cycle_ctx) {
    //   printf("scce_cycle_ctxsize: %d\n", scce_cycle_ctx_size);
    // }
    if (scce_cycle_ctx != 0) {
      actual_words++;
    }

    if (actual_words > 0 && scce_cycle_ctx_size <= _acyc_slots) {      
      actual_words--;
    }
    
    _stack_size_hist[actual_words]++;
  }
  else if (_dump_bit_stack_hist) {
    float actual_bits = _scce_cycle_ctxs.size() * _cycle_buffer_capacity
      + scce_cycle_ctx_size ;
    //int actual_words = ceil(actual_bits/64);
    //_stack_size_hist[actual_words]++;
    _stack_size_hist[actual_bits]++;
  }
  else {
    if (_fcce_ctx_pairs.find(key) == _fcce_ctx_pairs.end()) {
      if (_fcce_ctx_pairs.size() < 100000) {
        _fcce_ctx_pairs[key] = 0;
      }
      
      //printf("map size: %d, %d\n", _fcce_ctx_pairs.size(), _scce_cycle_ctxs.size());
    }
  }
    

  // if (scce_cycle_ctx) {
  //   _scce_cycle_ctxs.pop_back();
  // }

  _read_bytes[0] += 8;
  _read_bytes[1] += _scce_cycle_ctxs.size() * 8 + 8;

}
  

void reportCtxCollision(string &ctx, string &cv) {
  cerr << "Context collision: \n";
  cerr << ctx << "\n"
       << _ctxs[ctx] << "\n"
       << cv << "\n";
  
  backtrace_print(_bt_state, 2, stderr);
}

void reportCvCollision(string &cv, string &ctx) {
  cerr << "Cv collision: \n";
  cerr << cv << "\n"
       << _cvs[cv] << "\n"
       << ctx << "\n";
  backtrace_print(_bt_state, 2, stderr);
}

void checkCtx(int level) {
 string ctx = getBTCtxAsStr();
 string cv = getSCCECtxAsStr(level);

 if (_tz < 0) {
   //cout << "ctx: " << ctx << "cv: " << cv << "\n\n";
   //_tz++;
 }

 if (_ctxs.find(ctx) != _ctxs.end()) {
   if (cv != _ctxs[ctx]) {
     reportCtxCollision(ctx, cv);
   }
 } else {
   _ctxs[ctx] = cv;
 }

 if (_cvs.find(cv) != _cvs.end()) {
   if (ctx != _cvs[cv]) {
     reportCvCollision(cv, ctx);
   }
 } else {
   _cvs[cv] = ctx;
 }
}

  // Like "0xabc"
size_t parseHexStr(string s) {
  return strtol(s.substr(2).c_str(), NULL, 16);
}

string split(string s, char delim, int index) {
  int p = s.find(delim);
  if (index == 0) {
    return s.substr(0, p);
  }
  else if (index == 1) {
    return s.substr(p+1);
  }
  else {
    assert(0);
  }
}

  void loadContexts() {
  // 0x1f 0x703b|0xb 0x4, (nil) 0x4, (nil) 0x4, (nil) 0x5, (nil) 0x4, (nil) 0x4, (nil) 0x4, (nil) 0x5, (nil) 0x3, (nil) 0x5,
  ifstream ifs(getenv("CONTEXT_FILE"));
  string line;

  printf("load: %s\n", getenv("CONTEXT_FILE"));
  size_t client_id, pcce_id;
  

  while(std::getline(ifs, line)) {
    vector<pair<CtxIDType, SiteIDType>> stack;
    string head = split(line, '|', 0);
    string site_idstr = split(head, ' ', 0);
    string pcce_idstr = split(head, ' ', 1);
    client_id = parseHexStr(site_idstr);
    pcce_id = parseHexStr(pcce_idstr);
    // printf("%p %p|", client_id, pcce_id);
    // fflush(stdout);
    string stackstr = split(line, '|', 1);
    
    while (stackstr.find(',') != string::npos) {
      int p = stackstr.find(',');
      string pair = stackstr.substr(0, p);
      size_t saved_pcce_id = parseHexStr(split(pair, ' ', 0));
      size_t edge_id = parseHexStr(split(pair, ' ', 1));
      // printf("%p %p, ", saved_pcce_id, edge_id);
      // fflush(stdout);
      auto pa = make_pair(saved_pcce_id, (SiteIDType)edge_id);
      stack.push_back(pa);

      if (stackstr.size() - p < 3) {
        break;
      }
      else {
        stackstr = stackstr.substr(p+2);
      }
      
    }

    //tuple<int, size_t, size_t, vector<size_t>> key(id, pcce_id, scce_cycle_ctx, _scce_cycle_ctxs);
    //PCCEKeyType key(client_id, pcce_id, stack);
    //_fcce_ctx_pairs[key] = 0;
  }
  ifs.close();
}

  void dumpContexts() {
    FILE* f = fopen("fcce_contexts_dump.out", "w");
    for (auto i: _fcce_ctx_pairs) {
      auto key = i.first;
      auto point_id = get<0>(key);
      if (point_id == 14277) {
          printCtxKey(key, f);
      }
      
    }
    fclose(f);
  }

  void dumpStackHist() {
    FILE* f = fopen("fcce_stack_histogram.out", "w");
    for (auto i: _stack_size_hist) {
      auto key = i.first;
      auto value = i.second;
      fprintf(f, "%llu %llu\n", key, value);
    }
    fclose(f);
  }

  void reportCtxNum(ofstream& ofs) {
    printf("reportCtxNum\n");
    ofs << "[SCCE]\n";
    const int num = 20000;
    int points[num] = {0};
    for (auto i: _fcce_ctx_pairs) {
      auto key = i.first;
      auto point_id = get<0>(key);
      if (point_id < num) {
        points[point_id]++;
      }
      
    }

    for (int i = 0; i < num; ++i) {
      if (points[i] > 1) {
        ofs << "  " << i << ": " << points[i] << "\n";
      }
      
    }
  }
  
void report() {
  ofstream ofs("SCCE.out");
  ofs << "[SCCE]\n";
  ofs << "  ctxs: " << _ctxs.size() << "\n"
       << "  cvs: " << _cvs.size() << "\n"
       << "  max stack: " << _scce_cycle_ctxs.capacity() << "\n"
      << "  final stack: " << _scce_cycle_ctxs.size() << "\n"
       << "  fcce hashes: " << _fcce_ctx_pairs.size() << "\n"
      << "  scce hashes: " << _scce_ctx_pairs.size() << "\n"
       << "  read ids: " << _read_bytes[0] << " " << _read_bytes[1] << "\n"
      << "  pushed: " << _pushed << "\n"
      << "  popped: " << _popped << "\n"
    ;

  if (_load_contexts) {
    ofs << "  ctx hits: " << _context_hit << "\n";
  }

  //printf("pcce_id: %llu, scce_id: %llu, stack: %p\n", pcce_id, scce_cycle_ctx, _scce_cycle_ctxs[0]);
  //reportCtxNum(ofs);
  //dumpContexts();
  ofs.close();
  if (_dump_contexts) {
    dumpContexts();
  }

  if (_dump_stack_hist || _dump_bit_stack_hist) {
    dumpStackHist();
  }

  //

  // ofs.open("cycle_edge_freqs.out");
  // for (auto i: _edge_freqs) {
  //   ofs << i.first << " " << i.second << '\n';
  // }
  // ofs.close();
}

}

extern "C" {

//=----------------- Marena Interfaces ---------------=//
void scce_push_edge(short edge_id) {
  _cycle_edges.push_back(edge_id);
  _pushed += 1;
}

void scce_pop_edge(short edge_id) {
  short last_pushed = _cycle_edges[_cycle_edges.size()-1];
  if (last_pushed != edge_id) {
    printf("should pop %p, but popped %p\n", edge_id, last_pushed);
    exit(2);
  }
  _cycle_edges.pop_back();
  _popped++;
}

  /*** For the naive Method ***/
  /* This method somehow has different numbers of contexts from PCCE,
     Not sure why yet */
  void scce_left_shift(int bits, int value) {
    if (_naive_mode == 0) {
      _scce_cycle_ctxs.push_back(value);
      _pushed++;
      //printf("stack: %d\n", _scce_cycle_ctxs.size());
    }
    else if (_naive_mode == 1) {
      // printf("scce_cycle_ctx_size: %d, bits: %d\n",
      //        scce_cycle_ctx_size, bits);
      //if (scce_cycle_ctx_size + bits > 64) {
    
      if (scce_cycle_ctx * pow(2, bits) > 0xffffffffffffffff) {
        _scce_cycle_ctxs.push_back(scce_cycle_ctx);
        _pushed++;
        scce_cycle_ctx = 0;
        scce_cycle_ctx_size = 0;
      }
      
      scce_cycle_ctx <<= bits;
      scce_cycle_ctx += value;
      scce_cycle_ctx_size += bits;

      // printf("1scce_cycle_ctx_size: %d, bits: %d\n",
      //        scce_cycle_ctx_size, bits);

      // if (scce_cycle_ctx_size >= 60) {
      //   _scce_cycle_ctxs.push_back(scce_cycle_ctx);
      //   _pushed++;
      //   scce_cycle_ctx = 0;
      //   scce_cycle_ctx_size = 0;
      // }

    }

    //_edge_freqs[edge_id]++;
  }

  void scce_right_shift(int bits) {
    if (_naive_mode == 0) {
      _scce_cycle_ctxs.pop_back();
    }
    else if (_naive_mode == 1) {
      //if (scce_cycle_ctx_size == 0) {
      if (scce_cycle_ctx == 0) {
        scce_cycle_ctx = _scce_cycle_ctxs[_scce_cycle_ctxs.size()-1];
        _scce_cycle_ctxs.pop_back();
      }
      

      //printf("before: %p\n", scce_cycle_ctx);
      scce_cycle_ctx >>= bits;
      //printf("after : %p\n", scce_cycle_ctx);
      scce_cycle_ctx_size -= bits;
      
            

      // if (_justpushed) {
      //   // assert(_scce_cycle_ctxs.size() > 0);
      //   // assert(scce_cycle_ctx == 0);
      //   scce_cycle_ctx = _scce_cycle_ctxs[_scce_cycle_ctxs.size()-1];
      //   _scce_cycle_ctxs.pop_back();
      //   scce_cycle_ctx_size = 64;
      //   _justpushed = false;
      // }
    }
  }

  // void scce_left_shift(short edge_id) {
//   if (scce_cycle_ctx_size == 64) {
//     printf("push %p, size: %d\n", scce_cycle_ctx, _scce_cycle_ctxs.size());
//     _scce_cycle_ctxs.push_back(scce_cycle_ctx);
//     _pushed++;
//     scce_cycle_ctx = 0;
//     scce_cycle_ctx_size = 0;
//   }
//   scce_cycle_ctx <<= 16;
//   scce_cycle_ctx += edge_id;
//   //printf("add: %p\n", edge_id);
//   scce_cycle_ctx_size += 16;
//   printf("ctx size push: %d\n", scce_cycle_ctx_size);
// }

// void scce_right_shift() {
//   if (scce_cycle_ctx_size == 0) {
//     assert(_scce_cycle_ctxs.size() > 0);
//     scce_cycle_ctx = _scce_cycle_ctxs[_scce_cycle_ctxs.size()-1];
//     _scce_cycle_ctxs.pop_back();
//     scce_cycle_ctx_size = 64;
//     printf("pop %p, size: %d\n", scce_cycle_ctx, _scce_cycle_ctxs.size());
//   }
  
//   scce_cycle_ctx >>= 16;
//   scce_cycle_ctx_size -= 16;
//   printf("ctx size pop: %d\n", scce_cycle_ctx_size);
//   //printf("remove: \n");
// }


  /*** For non-branch method ***/
  void scce_push_cycle_ctx(int bits, int value) {
    // if (_scce_cycle_ctxs.size()) {
    //   printf("before push %d, %p\n", _scce_cycle_ctxs.size(), scce_cycle_ctx);
    //   fflush(stdout);
      
    // }
      
    _cycle_buffer_capacity = scce_cycle_ctx_size;
    _scce_cycle_ctxs.push_back(scce_cycle_ctx);
    

    scce_cycle_ctx = 0;
    scce_cycle_ctx_size = 0;
    _pushed++;
  }

  void scce_pop_cycle_ctx(int id) {
    //printf("before pop %d\n", _scce_cycle_ctxs.size());
    scce_cycle_ctx = _scce_cycle_ctxs[_scce_cycle_ctxs.size()-1];
    _scce_cycle_ctxs.pop_back();
    

    // Before the push, it should be full
    // Now set back to full
    scce_cycle_ctx_size = _cycle_buffer_capacity;
    _popped++;
  }

  /*** For the Branch (Default) Method ***/
  // void scce_push_cycle_ctx(int bits, int value) {
  //   //printf("before push %d, %p, id: %d\n", _scce_cycle_ctxs.size(), scce_cycle_ctx, value);

  //   _cycle_buffer_capacity = scce_cycle_ctx_size;
  //   _scce_cycle_ctxs.push_back(scce_cycle_ctx);

    
  //   scce_cycle_ctx = value;
  //   scce_cycle_ctx_size = bits;
  //   _pushed++;
  // }

  // void scce_pop_cycle_ctx(int id) {
  //   //printf("before pop %d, pop id: %d\n", _scce_cycle_ctxs.size(), id);
  //   scce_cycle_ctx = _scce_cycle_ctxs[_scce_cycle_ctxs.size()-1];
  //   _scce_cycle_ctxs.pop_back();

  //   // Before the push, it should be full
  //   // Now set back to full
  //   scce_cycle_ctx_size = _cycle_buffer_capacity;
  // }


void init_scce() {
  _bt_state = backtrace_create_state(
      NULL, 1, bt_error_callback, NULL);
  
  char* slots = getenv("XPS_AcycSlots");
  if (slots) {
    _acyc_slots = atoi(slots);
    
  }
  printf("available acyclic slots: %d\n", _acyc_slots);
}

void exit_scce() {
  report();
}



void scce_check_ctx(int lv) {
  checkCtx(lv);
  //checkSCCECtx(lv);
}

void scce_detect_ctx(size_t id) {
  detectSCCECtx(id);
}

void ecce_detect_ctx(size_t id) {
  //detectECCECtx();
  if (_load_contexts) {
    if (_fcce_ctx_pairs.size() == 0) {
      loadContexts();
    }
    
  }
  detectSCCEShiftCtx(id);
}
//=----------------- IR Interfaces ---------------=//
}
