//
// Created by tzhou on 3/8/18.
//

#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <backtrace.h>
#include <backtrace-supported.h>
#include "marena.hpp"
#include "../CCECommons/Commons.hpp"
#include "../PCCE/Interface.hpp"

// ACCE
typedef char CtxSlotType;
typedef char CycleSlotType;

const int ctx_size = 128;
extern "C" {
extern CtxIDType hcce_id;
CtxSlotType xps_ctx[ctx_size];
CycleSlotType xps_cycles[ctx_size];
}

namespace {
map<string, string> _ctxs;
map<string, string> _cvs;
backtrace_state *_bt_state;
int _tz = 0;

string getACCECtxAsStr(int level) {
  string s;
  for (int i = 0; i < level; ++i) {
    if (i) {
      s += "_";
    }
    char chars[10];
    sprintf(chars, "%04x", xps_ctx[i]);
    s += std::to_string(i) + "_" + chars;
  }
  return s;
}

inline size_t checkACCECtx(int level) {
  size_t hash = 0;
  for (int i = 0; i < level; i += 8/sizeof(CtxSlotType)) {
    size_t* p = (size_t*)(xps_ctx + i);
    hash += *p;
  }

  for (int i = 0; i < level; i += 8/sizeof(CycleSlotType)) {
    size_t* p = (size_t*)(xps_cycles + i);
    hash += *p;
  }
  return hash;
}

string getHCCECtxAsStr(int level) {
  return getPCCECtxAsStr() + getACCECtxAsStr(level);
}

void reportCtxCollision(string &ctx, string &cv) {
  cerr << "Context collision: \n";
  cerr << ctx << "\n"
       << _ctxs[ctx] << "\n"
       << cv << "\n";

  backtrace_print(_bt_state, 2, stderr);
}

void reportCvCollision(string &cv, string &ctx) {
  cerr << "Cv collision: \n";
  cerr << cv << "\n"
       << _cvs[cv] << "\n"
       << ctx << "\n";
  backtrace_print(_bt_state, 2, stderr);
}

void checkCtx(int level) {
  string ctx = getBTCtxAsStr();
  string cv = getHCCECtxAsStr(level);

  
  if (_tz < 0) {
    //cout << "ctx: " << ctx << "cv: " << cv << "\n\n";
    //_tz++;
  }
//
  if (_ctxs.find(ctx) != _ctxs.end()) {
    if (cv != _ctxs[ctx]) {
      reportCtxCollision(ctx, cv);
    }
  } else {
    _ctxs[ctx] = cv;
  }

  if (_cvs.find(cv) != _cvs.end()) {
    if (ctx != _cvs[cv]) {
      reportCvCollision(cv, ctx);
    }
  } else {
    _cvs[cv] = ctx;
  }
}

void report() {
  cout << "[HCCE]\n";
  cout << "  ctxs: " << _ctxs.size() << "\n"
       << "  cvs: " << _cvs.size() << "\n"
                                 ;
}

}

extern "C" {

//=----------------- Marena Interfaces ---------------=//
void init_hcce() {
  _bt_state = backtrace_create_state(
      NULL, 1, bt_error_callback, NULL);
}

void exit_hcce() {
  report();
}


void hcce_check_ctx(int lv) {
  //checkCtx(lv);
  checkACCECtx(lv);
}

//=----------------- IR Interfaces ---------------=//
}
