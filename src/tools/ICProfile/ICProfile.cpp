#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <map>
#include <set>

using namespace std;

static map<void*, char*> funcs;
static map<int, set<void*>> ics;
//static map<int, size_t> callsites;
size_t call_count = 0;

//=-------------------------- Interfaces =--------------------------//
extern "C" {
  void init_ic_profile() {
    
  }

  void dump_map_callsites() {
    FILE* f = fopen("cs_profile.out", "w");
    // for (auto cs: callsites) {
    //   fprintf(f, "%llu %llu\n", cs.first, cs.second);
    // }
    fprintf(f, "%llu\n", call_count);
    fclose(f);
  }

  void exit_ic_profile() {
    // dump_map_callsites();
    // return;

    printf("dump ic profiles...\n");
    FILE* f = fopen("ic_profile.out", "w");

    for (auto it: ics) {
      auto& targets = it.second;
      for (auto t: targets) {
        fprintf(f, "ic: %d %p\n", it.first, t);
      }
    }

    for (auto it: funcs) {
      fprintf(f, "fn: %p %s\n", it.first, it.second);
    }

    fclose(f);
  }

  void xps_log_call(int id) {
    //callsites[id]++;
    call_count++;
  }

  void xps_log_ic(int id, void* addr) {
    auto& targets = ics[id];
    if (targets.find(addr) != targets.end()) {
      return;
    }

    targets.insert(addr);
  }

  void xps_log_func(void* name, void* addr) {
    funcs[addr] = (char*)name;
  }
}
