//
// Created by tzhou on 2/24/18.
//
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <fstream>
#include <set>
#include <utility>      // std::pair, std::make_pair
#include <backtrace.h>
#include <backtrace-supported.h>
#include "marena.hpp"
#include "Interface.hpp"
#include <cassert>

extern "C" {
CtxIDType pcce_id = 0;
}

struct PCCEKey {
  int client_id;
  CtxIDType pcce_id;
  vector<pair<CtxIDType, SiteIDType>> stack;

  PCCEKey() {}
  
  PCCEKey(int client_id, CtxIDType pcce_id, vector<pair<CtxIDType, SiteIDType>>& stack) {
    this->client_id = client_id;
    this->pcce_id = pcce_id;
    this->stack =stack;
  }
};

typedef tuple<int, CtxIDType, vector<pair<CtxIDType, SiteIDType>>> PCCEKeyType;

namespace {
vector<pair<CtxIDType, SiteIDType>> _ctx_stack;
map<string, string> _ctxs;
map<string, string> _cvs;
set<string> _ctx_collisions;
set<string> _cv_collisions;
backtrace_state *_bt_state;
int _tz = 0;

unordered_map<size_t, int> _ctx_hashes;
map<PCCEKeyType, int> _ctx_pairs;
map<size_t, size_t> _stack_size_hist;

size_t _ctx_count = 0;
size_t _read_bytes[2] = {0};
size_t _pushed = 0;
size_t _popped = 0;
bool _use_sampling = false;
bool _dump_contexts = 0;
bool _load_contexts = false;
bool _dump_stack_hist = 0;

bool _context_hit = 0;

void reportCtxCollision(string &ctx, string &cv) {
  cerr << "Context collision: \n";
  cerr << ctx << "\n"
       << _ctxs[ctx] << "\n"
       << cv << "\n";

  backtrace_print(_bt_state, 2, stderr);
}

void reportCvCollision(string &cv, string &ctx) {
  cerr << "Cv collision: \n";
  cerr << cv << "\n"
       << _cvs[cv] << "\n"
       << ctx << "\n";
  backtrace_print(_bt_state, 2, stderr);
}

void checkCtx() {
  string ctx = getBTCtxAsStr();
  string cv = getPCCECtxAsStr();

  // backtrace_print(_bt_state, 1, stderr);
  // fprintf(stderr, "\n\n");
  // return;

  if (_tz < 0) {
    //cout << "ctx: " << ctx << "cv: " << cv << "\n\n";
    //_tz++;
  }
//
  if (_ctxs.find(ctx) != _ctxs.end()) {
    if (cv != _ctxs[ctx]) {
      if (_ctx_collisions.find(ctx) == _ctx_collisions.end()) {
        reportCtxCollision(ctx, cv);
      }
      _ctx_collisions.insert(ctx);

    }
  } else {
    _ctxs[ctx] = cv;
  }

  if (_cvs.find(cv) != _cvs.end()) {
    if (ctx != _cvs[cv]) {
      if (_cv_collisions.find(cv) == _cv_collisions.end()) {
        reportCvCollision(cv, ctx);
      }
      _cv_collisions.insert(cv);

    }
  } else {
    _cvs[cv] = ctx;
  }
}
} // end namespace

string getPCCECtxAsStr() {
  string s;
  for (auto i: _ctx_stack) {
    s += to_string(i.first) + ", " + to_string(i.second) + " | ";
  }
  s += to_string(pcce_id);
  return s;
}

void printCtxKey(PCCEKeyType key, FILE* f) {
  fprintf(f, "%p %p|", get<0>(key), get<1>(key));
  auto stack = get<2>(key);
  if (!stack.empty()) {
    for (auto i: stack) {
      fprintf(f, "%p %p, ", i.first, i.second);
    }
  }
  fprintf(f, "\n");
}

size_t detectPCCECtx(int id) {
  if (_use_sampling) {
    if (_ctx_count != 0) {
      _ctx_count++;
      if (_ctx_count == 200000) {
        _ctx_count = 0;
      }
      return 0;
    }  
    _ctx_count++;
  }
  
  size_t hash = 0;
  size_t bytes = 0;

  // auto key = tuple<int, CtxIDType,
  //                 vector<pair<CtxIDType, SiteIDType>>>
  //   (id, pcce_id, _ctx_stack);
  PCCEKeyType key(id, pcce_id, _ctx_stack);
  // tuple<int, CtxIDType,
  //                 vector<pair<CtxIDType, SiteIDType>>>
  //   key(id, pcce_id, _ctx_stack);

  if (_load_contexts) {
    if (_ctx_pairs.find(key) != _ctx_pairs.end()) {
      _context_hit++;
    }
  }
  else if (_dump_stack_hist) {
    _stack_size_hist[_ctx_stack.size()]++;
  }
  else {
    if (_ctx_pairs.find(key) == _ctx_pairs.end()) {
      if (_ctx_pairs.size() < 100000) {
        _ctx_pairs[key] = 0;
      }
      
      //printCtxKey(key);
    }

  }
   
  //_ctx_collisions.insert(getBTCtxAsStr());
  //bytes = _ctx_stack.size() * 2 + 1;
  _read_bytes[0] += 8;
  _read_bytes[1] += _ctx_stack.size() * 10;
  //printf("bytes: %lld\n", _ctx_stack.size());
  return hash;
}

// Like "0xabc"
size_t parseHexStr(string s) {
  return strtol(s.substr(2).c_str(), NULL, 16);
}

string split(string s, char delim, int index) {
  int p = s.find(delim);
  if (index == 0) {
    return s.substr(0, p);
  }
  else if (index == 1) {
    return s.substr(p+1);
  }
  else {
    assert(0);
  }
}

void loadContexts() {
  // 0x1f 0x703b|0xb 0x4, (nil) 0x4, (nil) 0x4, (nil) 0x5, (nil) 0x4, (nil) 0x4, (nil) 0x4, (nil) 0x5, (nil) 0x3, (nil) 0x5,
  ifstream ifs(getenv("CONTEXT_FILE"));
  string line;

  
  printf("load: %s\n", getenv("CONTEXT_FILE"));
  
  size_t client_id, pcce_id;
  

  while(std::getline(ifs, line)) {
    vector<pair<CtxIDType, SiteIDType>> stack;
    string head = split(line, '|', 0);
    string site_idstr = split(head, ' ', 0);
    string pcce_idstr = split(head, ' ', 1);
    client_id = parseHexStr(site_idstr);
    pcce_id = parseHexStr(pcce_idstr);
    // printf("%p %p|", client_id, pcce_id);
    // fflush(stdout);
    string stackstr = split(line, '|', 1);
    
    while (stackstr.find(',') != string::npos) {
      int p = stackstr.find(',');
      string pair = stackstr.substr(0, p);
      size_t saved_pcce_id = parseHexStr(split(pair, ' ', 0));
      size_t edge_id = parseHexStr(split(pair, ' ', 1));
      // printf("%p %p, ", saved_pcce_id, edge_id);
      // fflush(stdout);
      auto pa = make_pair(saved_pcce_id, (SiteIDType)edge_id);
      stack.push_back(pa);

      if (stackstr.size() - p < 3) {
        break;
      }
      else {
        stackstr = stackstr.substr(p+2);
      }
      
    }
    
    PCCEKeyType key(client_id, pcce_id, stack);
    _ctx_pairs[key] = 0;
  }
  ifs.close();
}

void dumpContexts() {
  FILE* f = fopen("pcce_contexts_dump.out", "w");
  for (auto i: _ctx_pairs) {
    auto key = i.first;
    auto point_id = get<0>(key);
    if (point_id == 14277)
      printCtxKey(key, f);
  }
  fclose(f);
}

void dumpStackHist() {
  FILE* f = fopen("pcce_stack_histogram.out", "w");
  for (auto i: _stack_size_hist) {
    auto key = i.first;
    auto value = i.second;
    fprintf(f, "%llu %llu\n", key, value);
  }
  fclose(f);
}

void reportCtxNum(ofstream& ofs) {
  ofs << "[PCCE]\n";
  const int num = 20000;
  int points[num] = {0};
  for (auto i: _ctx_pairs) {
    auto key = i.first;
    auto point_id = get<0>(key);;
    
    if (point_id < num) {
      points[point_id]++;
    }
  }

  for (int i = 0; i < num; ++i) {
    if (points[i] > 1) {
      ofs << "  " << i << ": " << points[i] << "\n";
    }
    
  }

  ofs << "BT contexts: " << _ctx_collisions.size() << "\n";
  for (auto i: _ctx_collisions) {
    ofs << i << "\n";
  }
}

void report() {
  ofstream ofs("PCCE.out");
  ofs << "[PCCE]\n"
      << "  ctxs: " << _ctxs.size() << "\n"
      << "  cvs: " << _cvs.size() << "\n"
      << "  ctx collisions: " << _ctx_collisions.size() << "\n"
      << "  cv collisions: " << _cv_collisions.size() << "\n"
      << "  max stack: " << _ctx_stack.capacity() << "\n"
      << "  final stack: " << _ctx_stack.size() << "\n"
      << "  hashes: " << _ctx_pairs.size() << "\n"
      << "  read ids: " << _read_bytes[0] << " " << _read_bytes[1] << "\n"
      << "  pushed: " << _pushed << "\n"
      << "  popped: " << _popped << "\n"
    ;

  if (_load_contexts) {
    ofs << "  ctx hits: " << _context_hit << "\n";
  }

  //reportCtxNum(ofs);
  //dumpContexts();
  ofs.close();

  if (_dump_contexts) {
    dumpContexts();
  }

  if (_dump_stack_hist) {
    dumpStackHist();
  }
}


extern "C" {

//=----------------- Marena Interfaces ---------------=//
void init_pcce() {
  pcce_id = 0;
  _bt_state = backtrace_create_state(
      NULL, 1, bt_error_callback, NULL);
  
}

void exit_pcce() {
  report();
}

void pcce_check_ctx() {
  checkCtx();
}

void pcce_detect_ctx(size_t id) {
  //checkCtx();
  if (_load_contexts) {
    if (_ctx_pairs.size() == 0) {
      loadContexts();
    }
  }

  detectPCCECtx(id);
}

//=----------------- IR Interfaces ---------------=//
  void pcce_print_hello(short i) {
    printf("pcce hello %d!\n", i);
    fflush(stdout);
  }

void pcce_push_ctx(size_t site) {

  //printf("pcce push ctx\n");
  auto pa = make_pair(pcce_id, (SiteIDType)site);
  _ctx_stack.push_back(pa);
  pcce_id = 0;
  _pushed++;

  //printf("stack: %d\n", _ctx_stack.size());
  
  // if (site == 2548) {
  //   pcce_print_hello();
  // }
}

void pcce_pop_ctx() {
  //printf("pcce pop ctx\n");

  assert(_ctx_stack.size() > 0);
  pcce_id = _ctx_stack[_ctx_stack.size() - 1].first;
  _ctx_stack.pop_back();
  _popped++;
}

void pcce_pop_ctx1(size_t site) {

  assert(_ctx_stack.size() > 0);
  pcce_id = _ctx_stack[_ctx_stack.size() - 1].first;

  assert(site == _ctx_stack[_ctx_stack.size() - 1].second);
  _ctx_stack.pop_back();
  _popped++;
}

}
