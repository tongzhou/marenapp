//
// Created by tzhou on 11/11/17.
//

#ifndef MSI_TYPES_HPP
#define MSI_TYPES_HPP

#include <cstdint>

#define MAX_DATA_LEN 2048
#define MAX_CTRL_DATA_LEN 10

typedef int data_len_t;

//#define MSG_CTRL_LEN      sizeof(data_len_t)+2
#define MAX_MSG_LEN       (1024+3) // in ints
//#define MAX_CTRL_MSG_LEN  MAX_CTRL_DATA_LEN+MSG_CTRL_LEN

#endif //MSI_TYPES_HPP
